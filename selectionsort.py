def selection(alist):
    for fillslot in range(len(alist)-1, 0, 1):
        pos_of_max = 0
        for location in range(1, fillslot + 1):
            if alist[position] > alist[pos_of_max]:
                pos_of_max = location

    temp = alist[fillslot]
    alist[fillslot] = alist[pos_of_max]
    alist[pos_of_max] = temp


#  driver code
alist = [54, 26, 93, 17, 77, 31, 44, 55, 20]
selection(alist)
print(alist)
